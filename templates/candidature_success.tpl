{include file="../templates/header.tpl"}
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span>Votre candidature a bien été envoyée.</span></h1>
            <a href="/detail/{$username}" class="btn btn-primary mx-auto d-block">Voir ma candidature</a>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
{include file="../templates/footer.tpl"}
