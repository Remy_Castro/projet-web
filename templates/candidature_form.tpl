
{include file="../templates/header.tpl"}
<main class="h-auto">
    <form method="POST" enctype="multipart/form-data">
        <div class="infos_groupe container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Informations du groupe</h1>
                <div class="row card-body">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <p style="color:red">{$message['grp_name']|escape|default:''}</p>
                                    <label for="" class="">Nom du groupe</label>
                                    <input type="text" class="form-control" name="group_name" value="{$post['group_name']|escape|default:''}">
                                </div>
                                <div class="col">
                                    <p style="color:red">{$message['date']|escape|default:''}</p>
                                    <label for="creation_year" class="">Année de création</label>
                                    <select name="creation_year" id="creation_year" class="form-control">
                                        <option value='{$post['creation_year']|escape|default:''}'>{$post['creation_year']|escape|default:'- Année de création -'}</option>
                                        {for $i=1900;$i<=date("Y");$i++}
                                            <option value="{$i}">
                                                {$i}
                                            </option>
                                        {/for}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['scene']|escape|default:''}</p>
                            <label for="type_scene">Type de scene</label>
                            <select name="type_scene" id="type_scene" class="form-control">
                                <option value='{$post['type_scene']|escape|default:''}'>{$nom_scene|escape|default:'- Type de scene -'}</option>
                                {foreach $scene as $ligne}
                                    <option value="{$ligne[0]}">{$ligne[1]}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['presentation']|escape|default:''}</p>
                            <label for="presentation">Présentation du groupe</label>
                            <textarea id="presentation" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="presentation">{$post['presentation']|escape|default:''}</textarea>
                        </div>

                        <div class="form-group">
                            <p style="color:red">{$message['link_fb']|escape|default:''}</p>
                            <label for="site_fb">Site web ou page facebook</label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_webFB" id="site_fb" value="{$post['link_webFB']|escape|default:''}">
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['link_ytb']|escape|default:''}</p>
                            <label for="youtube">Adresse page youtube <span class="text-muted">(Facultatif)</span></label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_ytb" id="youtube" value="{$post['link_ytb']|escape|default:''}">
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <p style="color:red">{$message['dpt']|escape|default:''}</p>
                            <label for="departement">Departement d'origine</label>
                            <select name="dpt" id="departement" class="form-control">
                                <option value='{$post['dpt']|escape|default:''}'>{$post['dpt']|escape|default:'- Département - '}</option>
                                {foreach $departement as $ligne}
                                    <option value="{$ligne[0]}">{$ligne[0]} - {$ligne[1]}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['style_musical']|escape|default:''}</p>
                            <label for="">Style musical</label>
                            <input type="text" class="form-control" placeholder="Exemple : rock,punk..." name="style_music" value="{$post['style_music']|escape|default:''}">
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['experience']|escape|default:''}</p>
                            <label for="experience">Expérience scénique</label>
                            <textarea id="experience" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="experience">{$post['experience']|escape|default:''}</textarea>
                        </div>
                        <div class="form-group">
                            <p style="color:red">{$message['link_soundcloud']|escape|default:''}</p>
                            <label for="soundcloud">Adresse page soundcloud <span class="text-muted">(Facultatif)</span></label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_soundcloud" value="{$post['link_soundcloud']|escape|default:''}">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="infos_perso container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Informations individuelles</h1>
                <div class="row card-body">
                    <div class="col">
                        <div class="répresentant">
                            <p class="lead">Répresentant du groupe</p>

                            <div class="form-group">
                                <p style="color:red">{$message['lead_name']|escape|default:''}</p>
                                <label for="l-nom">Nom</label>
                                <input type="text" class="form-control" name="leader_name" id="l-nom" value="{$post['leader_name']|escape|default:''}">
                            </div>

                            <div class="form-group">
                                <p style="color:red">{$message['lead_prenom']|escape|default:''}</p>
                                <label for="l-prenom">Prénom</label>
                                <input type="text" class="form-control" name="leader_prenom" id="l-prenom" value="{$post['leader_prenom']|escape|default:''}">
                            </div>
                            <div class="form-group">
                                <p style="color:red">{$message['lead_adresse']|escape|default:''}</p>
                                <label for="l-adresse">Adresse</label>
                                <input type="text" class="form-control" name="leader_adress" id="l-adresse" value="{$post['leader_adress']|escape|default:''}">
                            </div>
                            <div class="form-group">
                                <p style="color:red">{$message['lead_code_postal']|escape|default:''}</p>
                                <label for="l-cod">Code postal</label>
                                <input type="text" class="form-control" name="leader_codPost" id="l-cod" value="{$post['leader_codPost']|escape|default:''}">
                            </div>
                            <div class="form-group">
                                <p style="color:red">{$message['lead_email']|escape|default:''}</p>
                                <label for="l-mail">Email</label>
                                <input type="text" class="form-control" name="leader_email" id="l-mail" value="{$post['leader_email']|escape|default:''}">
                            </div>
                            <div class="form-group">
                                <p style="color:red">{$message['lead_phone']|escape|default:''}</p>
                                <label for="l-phone">Téléphone</label>
                                <input type="text" class="form-control" name="leader_phone" placeholder="Exemple : 0683627199" id="l-phone" value="{$post['leader_phone']|escape|default:''}">
                            </div>
                        </div>
                        <div class="members-card">
                            <p class="lead mt-5 d-inline-block ml-5">Membre(s) du groupe  <span class="text-muted" style="font-size:16px">(les membres non confirmés ne sont pas enregistrés)</span></p>
                            <div class="group-btn d-flex ml-5">
                                <button type="button" class="btn add btn-success" onclick="add()">Ajouter un membre</button>
                                <button type="button" class="btn del btn-danger ml-2" onclick="del()">Supprimer un membre</button>
                            </div>
                            <div id="card-container" class="row justify-content-around">

                                <div class="card col-5 mt-3 member-card">
                                    <p class="text-center" style="color:red">{$message['confirm_membre']|escape|default:''}</p>
                                    <div class="card-title text-center">Membre 1</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nom</label>
                                            <input type="text" class="last_name form-control" name="member_nom"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="">Prénom</label>
                                            <input type="text" class="first_name form-control" name="member_prenom"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="">Instrument</label>
                                            <input type="text" class="instru form-control" name="member_instruments"  >
                                        </div>
                                        <div class="send_info">
                                            <button type="button" class="btn btn-secondary mx-auto d-block"  onclick="confirm_aMember()"  >Confirmer ce membre</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- input invisible qui permet de stocker tous les membres saisies et validés-->
                            <input type="text" name="info_membres" id="members_jsToHtml" class="form-control" style="display: none;">

                            </div>
                            <div class="row">
                                <div class="col-12 mx-auto mt-5">
                                    <div class="d-flex flex-row">
                                        <div class="form-check">
                                            <p style="color:red">{$message['statut_asso']|escape|default:''}</p>
                                            <p>Avez-vous un statut associatif ?  </p>
                                            <input type="radio" id="asso_yes" name="statut_asso" value="o">
                                            <label for="asso_yes">Oui</label>
                                            <input type="radio" id="asso_no" name="statut_asso" class="ml-2" value="n">
                                            <label for="asso_no">Non</label>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row mt-3">
                                        <div class="form-check">
                                            <p style="color:red">{$message['sacem']|escape|default:''}</p>
                                            <p>Êtes-vous inscrit à la SACEM ?</p>
                                            <input type="radio" id="sacem_yes" name="inscrit_sacem" class="ml-1" value="o">
                                            <label for="sacem_yes">Oui</label>
                                            <input type="radio" id="sacem_no" name="inscrit_sacem" class="ml-2" value="n">
                                            <label for="sacem_no">Non</label>

                                        </div>
                                    </div>
                                    <div class="d-flex flex-row mt-3">
                                        <div class="form-check">
                                            <p style="color:red">{$message['producteur']|escape|default:''}</p>
                                            <p>Êtes-vous producteur ?</p>
                                            <input type="radio" id="product_yes" name="is_producteur" class="ml-2" value="o">
                                            <label for="product_yes">Oui</label>
                                            <input type="radio" id="product_no" name="is_producteur" class="ml-2" value="n">
                                            <label for="product_no">Non</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="pieces-jointes container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Pièces à fournir</h1>
                <div class="row card-body">
                    <div class="col-12">
                        <p class="text-danger" style="font-size:18px;">(Taille maximale d'un fichier : 2MO)</p>
                        <div class="file_music">
                            <p>- Insérez trois de vos musiques <span class="text-muted">(MP3)</span></p>
                            <div class="form-group">
                                <span style="color:red">{$message['music1']|escape|default:''}</span>
                                <label for="music1">Musique 1 : </label>
                                <input type="file" name="fileMusic_1" id="music1">
                            </div>
                            <div class="form-group">
                                <span style="color:red">{$message['music2']|escape|default:''}</span>
                                <label for="music2">Musique 2 : </label>
                                <input type="file" name="fileMusic_2" id="music2">
                            </div>
                            <div class="form-group">
                                <span style="color:red">{$message['music3']|escape|default:''}</span>
                                <label for="music3">Musique 3 : </label>
                                <input type="file" name="fileMusic_3" id="music3">
                            </div>
                        </div>

                        <div class="picture">
                            <p>- Deux photos de groupe : <span class="text-muted">(JPEG - PNG, Résolution supérieure à 300 dpi)</span></p>
                            <div class="form-group">
                                <span style="color:red">{$message['img1']|escape|default:''}</span>
                                <label for="img1">Photo 1 : </label>
                                <input type="file" name="img_1" id="img1">
                            </div>

                            <div class="form-group">
                                <span style="color:red">{$message['img2']|escape|default:''}</span>
                                <label for="img2">Photo 2 : </label>
                                <input type="file" name="img_2" id="img2">
                            </div>
                        </div>
                        <div class="form-group">
                            <span style="color:red">{$message['f_tech']|escape|default:''}</span>
                            <label for="f_technique">Fiche technique <span class="text-muted">(PDF)</span></label>
                            <br>
                            <input type="file" id="f_techique" name="f_tech">
                        </div>
                        <div class="form-group">
                            <span style="color:red">{$message['d_presse']|escape|default:''}</span>
                            <label for="presse">- Dossier de presse <span class="text-muted">(PDF)</span></label>
                            <br>
                            <input type="file" id="presse" name="dossier_presse">
                        </div>
                        <div class="form-group">
                            <span style="color:red">{$message['setlist']|escape|default:''}</span>
                            <label for="sacem_pdf">- Document SACEM / SETLIST<span class="text-muted">(PDF)</span></label>
                            <br>
                            <input type="file" id="sacem_pdf" name="setlist" class="ml-2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="pb-5">
                <button type="submit" class="btn btn-primary d-block mx-auto mt-5 w-50">Envoyez sa candidature</button>
            </div>
        </div>
    </form>
</main>
<style>
    @media screen and (max-width: 550px){
        .members-card .lead, .members-card .group-btn{
            margin-left: 0 !important;
        }
        .member-card{
            margin-left:0 !important;
            flex-basis:100%;
            max-width:100%;
        }
        .infos_groupe,.infos_perso,.pieces-jointes{
            max-width:100% !important;
            padding:0 !important;
        }
        .row{
            margin-right:0;
        }
        .pieces-jointes input{ /* permet de supprimer le débordement causer par : "aucun fichier selectionné"*/
            width: 100%;
        }
    }
</style>

<script src="../script/ajout_membre.js"></script>
{include file="../templates/footer.tpl"}

