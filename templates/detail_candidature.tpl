{include file="../templates/header.tpl"}
    <section class="container mt-5 info pb-5">
        {if $error eq true}
            <p>Désolé, il n'existe pas de candidature à ce nom.</p>
        {else}
        <div class="card">
            <div class="card-header d-flex flex-column align-items-center">
                <h1 class="text-center">{$infoCandidature[1]}</h1>
                {* badges pour afficher résultat des boutons radios *}
                <div class="badges">
                    {if $infoCandidature[18] eq 'o'}
                        <span class="badge bg-warning text-dark">PRODUCTEUR</span>
                    {/if}
                    {if $infoCandidature[19] eq 'o'}
                        <span class="badge bg-warning text-dark">STATUT ASSOCIATIF</span>
                    {/if}
                    {if $infoCandidature[20] eq 'o'}
                        <span class="badge bg-warning text-dark">INSCRIT SACEM</span>
                    {/if}
                </div>
            </div>
            <div class="card-body">
                <div class="row files_elements">
                    <div class="col-12 col-6 col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="card-img pb-5 d-flex align-items-center h-100">
                            <img src="../data/uploads/{$infoCandidature[25]}" alt="img" class="w-75 d-block mx-auto">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 mx-auto">
                        {*  Musiques *}
                        <div class="musique-un">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/{$infoCandidature[22]}" type="audio/mp4">
                                <source src="../data/uploads/{$infoCandidature[22]}" type="audio/mpeg">
                            </audio>
                        </div>
                        <div class="musique-deux">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/{$infoCandidature[23]}" type="audio/mp4">
                                <source src="../data/uploads/{$infoCandidature[23]}" type="audio/mpeg">
                            </audio>

                        </div>
                        <div class="musique-trois">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/{$infoCandidature[24]}" type="audio/mp4">
                                <source src="../data/uploads/{$infoCandidature[24]}" type="audio/mpeg">
                            </audio>
                        </div>

                        {*  Fichiers photos *}
                        <div class="photos mt-2">
                            <div class="photos-un">
                                <p class="titre">Image</p>
                                <a href="../data/uploads/{$infoCandidature[25]}" target="_blank" class="ml-2 text-primary">{$infoCandidature[25]}</a>
                            </div>
                            <div class="photos-deux">
                                <p class="titre">Image</p>
                                <a href="../data/uploads/{$infoCandidature[26]}" target="_blank" class="ml-2 text-primary">{$infoCandidature[26]}</a>
                            </div>
                        </div>
                        {*  Fichiers pdf *}
                        <div class="pdf">
                            <div class="pdf-un">
                                <p class="titre text-right">PDF</p>
                                <a href="../data/uploads/{$infoCandidature[27]}" target="_blank" class="ml-2 text-primary">{$infoCandidature[27]}</a>
                            </div>
                            <div class="pdf-deux">
                                <p class="titre">PDF</p>
                                <a href="../data/uploads/{$infoCandidature[28]}" target="_blank" class="ml-2 text-primary">{$infoCandidature[28]}</a>
                            </div>
                            <div class="pdf-trois">
                                <p class="titre">PDF</p>
                                <a href="../data/uploads/{$infoCandidature[29]}" target="_blank" class="ml-2 text-primary">{$infoCandidature[29]}</a>
                            </div>
                        </div>
                    </div>
                <div class="card-content mt-3 pt-5 border-top w-100">
                    <div class="element">
                        <p class="titre text-right">Département</p>
                        <span>{$infoCandidature[2]}</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Scene </p>
                        <span>{$infoCandidature[3]} ({$infoCandidature[31]})</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Style </p>
                        <span>{$infoCandidature[4]}</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Création </p>
                        <span>{$infoCandidature[5]}</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Youtube </p>
                        <span>{$infoCandidature[15]|escape|default:'-'}</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Soundcloud </p>
                        <span>{$infoCandidature[16]|escape|default:'-'}</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Web </p>
                        <span>{$infoCandidature[17]}</span>
                    </div>
                    <div class="element" id="txt-long">
                        <p class="titre text-right">Présentation</p>
                        <p class="long_texte text-justify">{$infoCandidature[6]}</p>
                    </div>
                    <div class="element" id="txt-long">
                        <p class="titre text-right">Experiences</p>
                        <p class="long_texte text-justify">{$infoCandidature[7]}</p>
                    </div>
                    <hr>
                    <div class="card card-reponsable w-75 mx-auto mt-2">
                        <div class="card-header">
                            <p class="card-title font-weight-bold text-center w-100">Responsable</p>
                        </div>
                        <div class="card-body">
                            <div class="element">
                                <p class="titre text-left">Nom et prénom</p>
                                <span>{$infoCandidature[8]},{$infoCandidature[9]}</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Adresse</p>
                                <span>{$infoCandidature[10]}</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Code postal</p>
                                <span>{$infoCandidature[11]}</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Email</p>
                                <span>{$infoCandidature[12]}</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Téléphone</p>
                                <span>{$infoCandidature[13]}</span>
                            </div>
                        </div>
                    </div>

                    <div class="card card-groupe w-75 mx-auto mt-5">
                        <div class="card-header">
                            <p class="card-title font-weight-bold text-center w-100">Groupe</p>
                        </div>
                        <div class="card-body">
                            <div class="element d-flex flex-column">
                                {assign var=membres value=","|explode:$infoCandidature[14]}
                                {foreach from=$membres key=key item=membre}
                                    {assign var=key value=$key+1}
                                    <div class="card mt-3">
                                        <p class="titre text-center card-header bg-dark text-white">Membre : {$key}</p>
                                        <div class="card-body">
                                            {assign var=unMembre value="-"|explode:$membre}
                                            <p><strong>Nom</strong> : {$unMembre[0]}</p>
                                            <p><strong>Prénom</strong> : {$unMembre[1]}</p>
                                            <p><strong>Instrument</strong> : {$unMembre[2]}</p>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                    </div>

                </div>
            </div>
        </div>
        {/if}
        <style>
            img{
                min-width: 200px;
                max-width: 400px;
                border-radius: 20px;
            }
            .element{
                display: flex;
            }
            .element .titre,.files_elements .titre{
                display: inline;
                min-width: 100px !important;
                font-weight: bold;
            }
            .info span,.long_texte{
                margin-left: 20px;
            }
            .card-header .card-title{
                font-size: 30px;
            }

            @media screen and (max-width: 500px){
                .container{
                    padding: 0;
                }
                card{
                    width: 100%;
                }
                p{
                    font-size: 15px;
                }
                .card-groupe, .card-reponsable{
                    font-size: 15px;
                    width: 100% !important;
                }
                #txt-long{
                    display: block;
                }
                #txt-long p{
                    width: 75%;
                }
            }
        </style>
    </section>
{include file="../templates/footer.tpl"}
