<footer class="border border-dark pb-3 pb-sm-3 pb-md-0 pb-lg-3 bg-dark">
        <div class="container">
            <div class="row justify-content-center mt-3">
                <h3><a href="/" class="text-white"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></a></h3>
            </div>
            <div class="row justify-content-center mt-3">
                <div class=" col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white text-center">Accueil</a>
                </div>
                <div class="col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white">Partenaires</a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="candidature" class="text-white">Déposer sa candidature</a>
                </div>
            </div>
        </div>
  </footer>
  <style>
  @media screen and (max-width: 450px){
        footer .row{
            display:flex;
            justify-content : center;
            flex-direction:column;
            margin-right: 0 !important;
        }
        footer .menu{
            width: 100% !important;
            max-width: 100% !important;
            margin-top: 5px;
        }
        footer h3{
            text-align : center !important;
        }
    }
</style>
