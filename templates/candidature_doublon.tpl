{include file="../templates/header.tpl"}
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span>{$source}</span></h1>
            {if $source_nomCompte eq true}
                <a href="/connexion" class="btn btn-primary mx-auto d-block">Voir ma candidature</a>
            {/if}
        </div>

    </div>
</div>
<style>
    h1{
        font-size: 25px;
    }
</style>
{include file="../templates/footer.tpl"}
