{include file="../templates/header.tpl"}
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center"><span>Merci pour votre inscription chez</span><br> <div class="text-center d-block pb-5 pt-2"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></div></h1>
            <a href="/connexion" class="btn btn-primary mx-auto d-block">Me connecter</a>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
{include file="../templates/footer.tpl"}
