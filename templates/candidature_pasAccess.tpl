{include file="../templates/header.tpl"}
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span>Vous n'avez pas accès à cette candidature.</span></h1>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
{include file="../templates/footer.tpl"}
