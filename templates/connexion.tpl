    {include file="../templates/header.tpl"}
    <div class="container w-50 container-form my-auto">
        <h1 class="text-center mt-5">Se connecter</h1>
        <form action="/connexion" method="post" class="mt-5 w-100 pb-5">
            <div class="form-group">
                <p style="color: red">{$messages["duo"]|escape|default:''}</p>
                <p style="color: red">{$messages["nom"]|escape|default:''}</p>
                <label for="name">Nom</label>
                <input value="{$post['name']|escape|default:''}" type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <p style="color: red">{$messages["email"]|escape|default:''}</p>
                <p style="color: red">{$messages["no_email"]|escape|default:''}</p>
                <label for="email">Email</label>
                <input value="{$post['email']|escape|default:''}" type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <p style="color: red">{$messages["password"]|escape|default:''}</p>
                <p style="color: red">{$messages["mdp"]|escape|default:''}</p>
                <label for="password">Mot de passe</label>
                <input value="{$post['password']|escape|default:''}" type="password" class="form-control" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary d-block mx-auto">Se connecter</button>
        </form>
    </div>

    <style>

        label{
            font-size:calc(10px + 0.75vh);
        }
        @media screen and (max-width: 500px){
            h1{
                font-size: 25px;
            }
            .container-form{
                width: 100% !important;
            }
        }
    </style>
{include file="../templates/footer.tpl"}

