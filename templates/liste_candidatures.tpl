{include file="../templates/header.tpl"}
<div class="container center mt-5 pb-3">
    <div class="card">
        <div class="card-header p-3">

            <h1 class="text-center">Toutes les candidatures</h1>
        </div>
        <card class="body d-flex mt-5 pb-5 overflow-auto">
            <table class="w-75">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center"></th>
                    <th class="text-center">Nom groupe</th>
                    <th class="text-center">Departement</th>
                    <th class="text-center">Type de scène</th>
                    <th class="text-center">Style</th>
                    <th class="text-center">Année de création</th>
                    <th class="text-center">Présentation</th>
                    <th class="text-center">Expérience</th>
                    <th class="text-center">Nom du représentant</th>
                    <th class="text-center">Prénom du représentant</th>
                    <th class="text-center">Youtube</th>
                    <th class="text-center">Soundcloud</th>
                    <th class="text-center">Site ou page facebook</th>
                    <th class="text-center">Producteur </th>
                    <th class="text-center">Statut associatif</th>
                    <th class="text-center">Inscrit à la sacem</th>
                </tr>

                {foreach $liste as $key=>$ligne}
                    <tr>
                        <td class="text-center">{$ligne[0]}</td>
                        <td class="text-center"><a href="/detail/{$ligne[1]}" class="text-primary">Détail</a></td>
                        {* 1 à 24 : donc boucle for : sauf fichiers car cliquable *}
                        <td class="text-center"><p>{$ligne[1]}</p></td>
                        <td class="text-center"><p>{$ligne[2]}</p></td>
                        <td class="text-center"><p>{$ligne[3]}</p></td>
                        <td class="text-center"><p>{$ligne[4]}</p></td>
                        <td class="text-center"><p>{$ligne[5]}</p></td>
                        <td class="text-center"><p>{$ligne[6]}</p></td>
                        <td class="text-center"><p>{$ligne[7]}</p></td>
                        <td class="text-center"><p>{$ligne[8]}</p></td>
                        <td class="text-center"><p>{$ligne[9]}</p></td>
                        <td class="text-center"><p>{$ligne[15]}</p></td>
                        <td class="text-center"><p>{$ligne[16]}</p></td>
                        <td class="text-center"><p>{$ligne[17]}</p></td>
                        <td class="text-center"><p>{$ligne[18]}</p></td>
                        <td class="text-center"><p>{$ligne[19]}</p></td>
                        <td class="text-center"><p>{$ligne[20]}</p></td>
                <tr>
                {/foreach}


            </table>
        </card>
    </div>
</div>
<style>
    p{
        max-height: 100px;
        overflow: scroll;
    }
</style>

{include file="../templates/footer.tpl"}
