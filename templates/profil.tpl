 {include file="../templates/header.tpl"}
<div class="container pt-5 mt-5 w-50 pb-5">
    <h1 class="text-center pb-3">Vos informations de compte</h1>

    <div class="card mt-5">
        <div class="card-header">
          <h3 class="text-center">Identifiants</h3>
        </div>
        <div class="card-body">
          <p><strong>Nom d'utilisateur</strong> : {$username|escape|default:''}</p>
          <p><strong>Adresse mail</strong> : {$email|escape|default:''}</p>
        </div>
    </div>
    <div class="card mt-5">
        <div class="card-header">
          <h3 class="text-center">Candidatures</h3>
        </div>
        <div class="card-body">
          {if $responsable eq 'o'}
            <a class="btn btn-dark btn-candidature d-block mx-auto" href="/liste">Voir toutes les candidatures</a>
          {elseif $nb_candidature eq 1}
            <p>Vous avez déposé une candidature. </p>
            <a class="btn btn-dark btn-candidature d-block mx-auto mt-2" href="/detail/{$username}">Voir ma candidature</a>
          {else}
              <p>Vous n'avez aucune candidature d'enregistrée. </p>
              <a class="btn btn-dark form-control btn-candidature d-block mx-auto mt-2" href="/candidature">Déposer ma candidature</a>
          {/if}


        </div>
    </div>
</div>
 <style>
     .btn-candidature{
         max-width: 80%;
     }

     @media screen and (max-width: 500px){
         .container{
             width: 100% !important;
         }
     }
 </style>
 {include file="../templates/footer.tpl"}
