<?php
session_start();
date_default_timezone_set('Europe/Paris');


require 'include/flight/flight/Flight.php';
require 'include/smarty/libs/Smarty.class.php';

require 'include/pdo.php';

Flight::register('view','Smarty',array(),function($smarty){
    $smarty->template_dir = './templates/';
    $smarty->compile_dir = './templates_c/';
    $smarty->config_dir = './config/';
    $smarty->cache_dir = './cache/';

});
Flight::map('render', function($template,$data){
    Flight::view()->assign($data);
    Flight::view()->display($template);
});

require 'functions.php';

Flight::start();
