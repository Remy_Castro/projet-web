<?php
/* Smarty version 3.1.34-dev-7, created on 2020-11-22 09:17:36
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/incription.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fba2cb05484b2_94911315',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'adf0968b22e793478ad79205acbc51404a481c8d' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/incription.tpl',
      1 => 1606036196,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fba2cb05484b2_94911315 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en" style="height: 100%;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>S'inscrire</title>
</head>
<style>
    body{
        height: 100%
    }
</style>
<body>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('watchCandidature'=>$_smarty_tpl->tpl_vars['isAdmin']->value), 0, false);
?>
    <div class="container pt-5 mt-5 w-50">
        <h1 class="text-center">Se créer un compte</h1>
        <form action="register" method="post" class="mx-auto mt-5">
            <div class="form-group">
                <label for="name">Nom</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['password']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="password" class="form-control" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary d-block mx-auto">Sign up</button>
        </form>
    </div>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('watchCandidature'=>$_smarty_tpl->tpl_vars['isAdmin']->value), 0, false);
?>
</body>
</html>
<?php }
}
