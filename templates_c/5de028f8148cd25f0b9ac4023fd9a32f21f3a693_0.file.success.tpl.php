<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 15:26:17
  from '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fccf81945b520_33488845',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5de028f8148cd25f0b9ac4023fd9a32f21f3a693' => 
    array (
      0 => '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/success.tpl',
      1 => 1607268051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fccf81945b520_33488845 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container w-100 d-flex justify-content-center align-items-center">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center"><span>Merci pour votre inscription chez</span><br> <div class="text-center d-block pb-5 pt-2"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></div></h1>
            <a href="/connexion" class="btn btn-primary mx-auto d-block">Me connecter</a>
        </div>
    </div>

</div>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html>
<style>
    html{
        height 100% !important;
    }
    .container{
        height: 80%;
    }
    footer{
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
    }
</style><?php }
}
