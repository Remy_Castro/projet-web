<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-06 12:32:18
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/detail_candidature.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff59fc2319e50_15562912',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3e94a5e960eb61366d35a0b734518032ea06da6' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/detail_candidature.tpl',
      1 => 1609855185,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff59fc2319e50_15562912 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <section class="container mt-5 info pb-5">
        <?php if ($_smarty_tpl->tpl_vars['error']->value == true) {?>
            <p>Désolé, il n'existe pas de candidature à ce nom.</p>
        <?php } else { ?>
        <div class="card">
            <div class="card-header d-flex flex-column align-items-center">
                <h1 class="text-center"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[1];?>
</h1>
                                <div class="badges">
                    <?php if ($_smarty_tpl->tpl_vars['infoCandidature']->value[18] == 'o') {?>
                        <span class="badge bg-warning text-dark">PRODUCTEUR</span>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['infoCandidature']->value[19] == 'o') {?>
                        <span class="badge bg-warning text-dark">STATUT ASSOCIATIF</span>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['infoCandidature']->value[20] == 'o') {?>
                        <span class="badge bg-warning text-dark">INSCRIT SACEM</span>
                    <?php }?>
                </div>
            </div>
            <div class="card-body">
                <div class="row files_elements">
                    <div class="col-12 col-6 col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="card-img pb-5 d-flex align-items-center h-100">
                            <img src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[25];?>
" alt="img" class="w-75 d-block mx-auto">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 mx-auto">
                                                <div class="musique-un">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[22];?>
" type="audio/mp4">
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[22];?>
" type="audio/mpeg">
                            </audio>
                        </div>
                        <div class="musique-deux">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[23];?>
" type="audio/mp4">
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[23];?>
" type="audio/mpeg">
                            </audio>

                        </div>
                        <div class="musique-trois">
                            <p class="titre">Musique</p><br>
                            <audio controls>
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[24];?>
" type="audio/mp4">
                                <source src="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[24];?>
" type="audio/mpeg">
                            </audio>
                        </div>

                                                <div class="photos mt-2">
                            <div class="photos-un">
                                <p class="titre">Image</p>
                                <a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[25];?>
" target="_blank" class="ml-2 text-primary"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[25];?>
</a>
                            </div>
                            <div class="photos-deux">
                                <p class="titre">Image</p>
                                <a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[26];?>
" target="_blank" class="ml-2 text-primary"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[26];?>
</a>
                            </div>
                        </div>
                                                <div class="pdf">
                            <div class="pdf-un">
                                <p class="titre text-right">PDF</p>
                                <a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[27];?>
" target="_blank" class="ml-2 text-primary"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[27];?>
</a>
                            </div>
                            <div class="pdf-deux">
                                <p class="titre">PDF</p>
                                <a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[28];?>
" target="_blank" class="ml-2 text-primary"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[28];?>
</a>
                            </div>
                            <div class="pdf-trois">
                                <p class="titre">PDF</p>
                                <a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[29];?>
" target="_blank" class="ml-2 text-primary"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[29];?>
</a>
                            </div>
                        </div>
                    </div>
                <div class="card-content mt-3 pt-5 border-top w-100">
                    <div class="element">
                        <p class="titre text-right">Département</p>
                        <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[2];?>
</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Scene </p>
                        <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[3];?>
 (<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[31];?>
)</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Style </p>
                        <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[4];?>
</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Création </p>
                        <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[5];?>
</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Youtube </p>
                        <span><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['infoCandidature']->value[15], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '-' : $tmp);?>
</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Soundcloud </p>
                        <span><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['infoCandidature']->value[16], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '-' : $tmp);?>
</span>
                    </div>
                    <div class="element">
                        <p class="titre text-right">Web </p>
                        <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[17];?>
</span>
                    </div>
                    <div class="element" id="txt-long">
                        <p class="titre text-right">Présentation</p>
                        <p class="long_texte text-justify"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[6];?>
</p>
                    </div>
                    <div class="element" id="txt-long">
                        <p class="titre text-right">Experiences</p>
                        <p class="long_texte text-justify"><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[7];?>
</p>
                    </div>
                    <hr>
                    <div class="card card-reponsable w-75 mx-auto mt-2">
                        <div class="card-header">
                            <p class="card-title font-weight-bold text-center w-100">Responsable</p>
                        </div>
                        <div class="card-body">
                            <div class="element">
                                <p class="titre text-left">Nom et prénom</p>
                                <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[8];?>
,<?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[9];?>
</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Adresse</p>
                                <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[10];?>
</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Code postal</p>
                                <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[11];?>
</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Email</p>
                                <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[12];?>
</span>
                            </div>
                            <div class="element">
                                <p class="titre text-left">Téléphone</p>
                                <span><?php echo $_smarty_tpl->tpl_vars['infoCandidature']->value[13];?>
</span>
                            </div>
                        </div>
                    </div>

                    <div class="card card-groupe w-75 mx-auto mt-5">
                        <div class="card-header">
                            <p class="card-title font-weight-bold text-center w-100">Groupe</p>
                        </div>
                        <div class="card-body">
                            <div class="element d-flex flex-column">
                                <?php $_smarty_tpl->_assignInScope('membres', explode(",",$_smarty_tpl->tpl_vars['infoCandidature']->value[14]));?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['membres']->value, 'membre', false, 'key');
$_smarty_tpl->tpl_vars['membre']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['membre']->value) {
$_smarty_tpl->tpl_vars['membre']->do_else = false;
?>
                                    <?php $_smarty_tpl->_assignInScope('key', $_smarty_tpl->tpl_vars['key']->value+1);?>
                                    <div class="card mt-3">
                                        <p class="titre text-center card-header bg-dark text-white">Membre : <?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</p>
                                        <div class="card-body">
                                            <?php $_smarty_tpl->_assignInScope('unMembre', explode("-",$_smarty_tpl->tpl_vars['membre']->value));?>
                                            <p><strong>Nom</strong> : <?php echo $_smarty_tpl->tpl_vars['unMembre']->value[0];?>
</p>
                                            <p><strong>Prénom</strong> : <?php echo $_smarty_tpl->tpl_vars['unMembre']->value[1];?>
</p>
                                            <p><strong>Instrument</strong> : <?php echo $_smarty_tpl->tpl_vars['unMembre']->value[2];?>
</p>
                                        </div>
                                    </div>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                    </div>

                </div>
            </div>
        </div>
        <?php }?>
        <style>
            img{
                min-width: 200px;
                max-width: 400px;
                border-radius: 20px;
            }
            .element{
                display: flex;
            }
            .element .titre,.files_elements .titre{
                display: inline;
                min-width: 100px !important;
                font-weight: bold;
            }
            .info span,.long_texte{
                margin-left: 20px;
            }
            .card-header .card-title{
                font-size: 30px;
            }

            @media screen and (max-width: 500px){
                .container{
                    padding: 0;
                }
                card{
                    width: 100%;
                }
                p{
                    font-size: 15px;
                }
                .card-groupe, .card-reponsable{
                    font-size: 15px;
                    width: 100% !important;
                }
                #txt-long{
                    display: block;
                }
                #txt-long p{
                    width: 75%;
                }
            }
        </style>
    </section>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
