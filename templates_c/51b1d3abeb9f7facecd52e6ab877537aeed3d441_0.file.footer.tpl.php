<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 13:59:54
  from '/Users/rems/Desktop/projet-web/templates/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fcce3dab18363_75926789',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '51b1d3abeb9f7facecd52e6ab877537aeed3d441' => 
    array (
      0 => '/Users/rems/Desktop/projet-web/templates/footer.tpl',
      1 => 1607262825,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fcce3dab18363_75926789 (Smarty_Internal_Template $_smarty_tpl) {
?><footer class="border border-dark pb-3 pb-sm-3 pb-md-0 pb-lg-3 bg-dark">
        <div class="container">
            <div class="row justify-content-center mt-3">
                <h3><a href="/" class="text-white">Festi'Music</a></h3>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-2 col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white text-center">Accueil</a>
                </div>
                <div class="col-2 col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white">Partenaires</a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="candidature" class="text-white">Déposer sa candidature</a>
                </div>
            </div>
        </div>
  </footer>
  <style>
  @media screen and (max-width: 450px){
        footer .row{
            justify-content : center;
            flex-direction:column;
        }
        footer .menu{
            width:100% important;
            max-width: 100% !important;
            margin-top: 5px;
        }
        footer h3{
            text-align : center !important;
        }
    }
</style><?php }
}
