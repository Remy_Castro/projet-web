<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 14:40:08
  from '/Users/rems/Desktop/projet-web/templates/connexion.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fcced485800b5_66244554',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fcf7c42668aaeee6c9d0117ca88c5387a6cef2d2' => 
    array (
      0 => '/Users/rems/Desktop/projet-web/templates/connexion.tpl',
      1 => 1607265606,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fcced485800b5_66244554 (Smarty_Internal_Template $_smarty_tpl) {
?>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <div class="container w-50 container-form my-auto">
        <h1 class="text-center mt-5">Se connecter</h1>
        <form action="/connexion" method="post" class="mt-5 w-100 pb-5">
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["duo"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p> 
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["nom"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <label for="name">Nom</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["email"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["no_email"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <label for="email">Email</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['email'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["password"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["mdp"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p> 
                <label for="password">Mot de passe</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['password'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="password" class="form-control" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary d-block mx-auto">Se connecter</button>
        </form>
    </div>
        <?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html>
<style>

    label{
        font-size:calc(12px + 1.5vh);

    }
    @media screen and (max-width: 300px){
        footer .row{
            justify-content : center;
            flex-direction:column;
        }
        h1{
            font-size: 30px;
        }
        .container-form{
            width: 100% !important;
        }
    }


</style>
<?php }
}
