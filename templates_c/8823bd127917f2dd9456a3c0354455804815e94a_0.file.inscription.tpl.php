<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 13:34:41
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/inscription.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff30b6128b530_56415091',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8823bd127917f2dd9456a3c0354455804815e94a' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/inscription.tpl',
      1 => 1609155570,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff30b6128b530_56415091 (Smarty_Internal_Template $_smarty_tpl) {
?>    <?php $_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <div class="container w-50 container-form my-auto">
        <h1 class="text-center mt-5">Se créer un compte</h1>
        <form action="/inscription" method="post" class="mt-5 w-100 pb-5">
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["error_nom"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <label for="name">Nom</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["error_email"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <label for="email">Email</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['email'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <p style="color: red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['messages']->value["error_password"], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                <label for="password">Mot de passe</label>
                <input value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['password'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
" type="password" class="form-control" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary d-block mx-auto">Créer un compte</button>
        </form>
    </div>

    <style>
        label{
            font-size:calc(10px + 0.75vh);
        }

        @media screen and (max-width: 500px){
            h1{
                font-size: 25px;
            }
            .container-form{
                width: 100% !important;
            }
        }
    </style>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
