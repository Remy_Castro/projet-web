<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 15:27:08
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff325bc88e203_16531614',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2d0fed455cf08b6cb9590c67466728ff1424023' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_success.tpl',
      1 => 1609770384,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff325bc88e203_16531614 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span>Votre candidature a bien été envoyée.</span></h1>
            <a href="/detail/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="btn btn-primary mx-auto d-block">Voir ma candidature</a>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
