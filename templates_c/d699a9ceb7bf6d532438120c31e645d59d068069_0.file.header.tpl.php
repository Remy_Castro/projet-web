<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 15:21:00
  from '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fccf6dc2f6533_78931287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd699a9ceb7bf6d532438120c31e645d59d068069' => 
    array (
      0 => '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/header.tpl',
      1 => 1607268052,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fccf6dc2f6533_78931287 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr"">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://kit.fontawesome.com/b23d86535f.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="../style/style.css" type="text/css">
    <title>Espace de connexion</title>
</head>
<style>
    body{
        height: 100%;
    }
</style>
<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="/" class="navbar-brand"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>

                </button>
                <div class="collapse navbar-collapse justify-content-around" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item ml-4 mr-4 mt-1">
                            <?php echo $_smarty_tpl->tpl_vars['data']->value['link'];?>

                        </li>
                        <li class="nav-item ml-4 mr-4 mt-1">
                            <?php echo $_smarty_tpl->tpl_vars['data']->value['link_2'];?>

                        </li>
                        <li class="nav-item ml-4 mr-4 mt-1">
                            <?php echo (($tmp = @$_smarty_tpl->tpl_vars['lien_responsable']->value)===null||$tmp==='' ? '' : $tmp);?>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

<?php }
}
