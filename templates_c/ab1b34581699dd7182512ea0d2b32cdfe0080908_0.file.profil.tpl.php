<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 15:29:36
  from '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/profil.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fccf8e0079a87_05036765',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ab1b34581699dd7182512ea0d2b32cdfe0080908' => 
    array (
      0 => '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/profil.tpl',
      1 => 1607268051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fccf8e0079a87_05036765 (Smarty_Internal_Template $_smarty_tpl) {
?> <?php $_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container pt-5 mt-5 w-50">
        <h1 class="text-center pb-3">Vos informations de compte</h1>

        <div class="card mt-5">
            <div class="card-header">
              <h3 class="text-center">Identifiants</h3>
            </div>
            <div class="card-body">
              <p><strong>Nom d'utilisateur</strong> : <?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['username']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
              <p><strong>Adresse mail</strong> : <?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
            </div>
        </div>
        <div class="card mt-5">
            <div class="card-header">
              <h3 class="text-center">Candidatures</h3>
            </div>
            <div class="card-body h-auto">
              <?php if ($_smarty_tpl->tpl_vars['responsable']->value == 'o') {?>
                <a class="btn btn-dark btn-candidature d-block mx-auto" href="#">Voir toutes les candidatures</a>
              <?php } else { ?>
                <p>Vous n'avez aucune candidature d'enregistrée. </p>
                <a class="btn btn-dark form-control btn-candidature d-block mx-auto" href="/candidature">Déposer ma candidature</a>
              <?php }?>     
              <?php if ('condition') {?>
                
              <?php } else { ?>
                
              <?php }?>

            </div>
        </div>
        
</div>
 <?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 <style>
  .btn-candidature{
    max-width: 80%;
  }
 </style>
 <?php }
}
