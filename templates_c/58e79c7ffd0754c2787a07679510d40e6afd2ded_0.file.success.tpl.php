<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 15:02:08
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff31fe02ebe50_48490440',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '58e79c7ffd0754c2787a07679510d40e6afd2ded' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/success.tpl',
      1 => 1609158524,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff31fe02ebe50_48490440 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center"><span>Merci pour votre inscription chez</span><br> <div class="text-center d-block pb-5 pt-2"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></div></h1>
            <a href="/connexion" class="btn btn-primary mx-auto d-block">Me connecter</a>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
