<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 15:29:33
  from '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fccf8ddeda182_87957884',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'acf49104e760395b56b883a39ec3508f39a82696' => 
    array (
      0 => '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/index.tpl',
      1 => 1607268051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fccf8ddeda182_87957884 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<main>
    <section class="container-fluid">
        <div class="content-background row">
            <div class="content-title d-flex flex-column justify-content-center align-items-center">
                <h1 class="text-white display-3 text-center">Festi'Music.</h1>
                <p class="text-center">Le plus grand festival de musique associatif de France.</p>
                <a href="candidature" class="btn-candidater btn btn-secondary mt-3 text-center">Déposer sa candidature</a>
            </div>
        </div>
    </section>

    <section class="section-cards pt-3">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-header text-center">Qu'est ce que Festi'Music ?</div>
                        <div class="card-body">
                            <h6>Présentation :</h6>
                            <p class="text-muted">Festi'Music est un festival de musique qui existe depuis 1998. Grâce à sa notoriété, il peut s'agir de tremplin pour les jeunes
                            artistes. </p>

                            <h6>Proche des malades : </h6>
                            <p class="text-muted">Tout l'argent collecté par les dons sera reversé aux malades</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt-5 mt-lg-0 mt-md-0 mt-">
                    <div class="card">
                        <div class="card-header text-center">Dates essentielles</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="d-flex">
                                        <div class="icone text-info">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                                </svg>
                                        </div>
                                        <p class="ml-2">Ouverture des candidatures : <span class="text-info">10/12/2020</span></p>
                                    </div>
                                    <div class="d-flex">
                                        <div class="icone text-info">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                                </svg>
                                        </div>
                                        <p class="ml-2">Fermeture des candidatures : <span class="text-info">01/04/2021</span></p>
                                    </div>
                                    <div class="d-flex">
                                        <div class="icon text-info">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </div>
                                        <p class="ml-2">Début de la préparation du festival : <span class="text-info">01/05/2021</span></p>
                                    </div>
                                    <div class="d-flex">
                                        <div class="icone text-info">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </div>
                                        <p class="ml-2">Début du festival :  <span class="text-info"> 01/07/2021</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-responsable mt-3 border-top mt-5 pt-3">
        <div class="container">
            <div class="row pb-4">
                <div class="col-12 text-center">
                    <h2>Les reponsables</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 col-lg-4 pb-5 pb-sm-5 pb-md-0 pb-lg-0">
                    <div class="card">
                        <div class="card-img-top bg-dark">
                            <img src="../images/male1-icon.png" alt="responsable-photo" class="w-25 d-block mx-auto">
                        </div>
                        <div class="card-body text-center text-lg-center text-md-left text-sm-center">
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Nom : <span class="responsable-result text-muted">Charles</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Fonction : <span class="responsable-result text-muted">Maire</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Mail : <span class="responsable-result text-muted">maire.ch@gmail.com</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-img-top bg-dark">
                            <img src="../images/female1-icon.png" alt="responsable-photo" class="w-25 d-block mx-auto">
                        </div>
                        <div class="card-body text-lg-center text-md-left text-sm-left">
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Nom : <span class="responsable-result text-muted">Patricia</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Fonction : <span class="responsable-result text-muted">Maire adjointe</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Mail : <span class="responsable-result text-muted">patri.ch@gmail.com</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-img-top bg-dark">
                            <img src="../images/male2-icon.png" alt="responsable-photo" class="w-25 d-block mx-auto">
                        </div>
                        <div class="card-body text-lg-center text-md-left text-sm-left">
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Nom : <span class="responsable-result text-muted">Jules</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Fonction : <span class="responsable-result text-muted">Fondateur</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="responsable-info">Mail : <span class="responsable-result text-muted">jules.ch@gmail.com</span></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>  <!-- fin row -->

        </div>
    </section>

    <section class="section-reseaux border-top pt-2 pb-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Nous suivre</h2>
                </div>
            </div>
            <div class="row justify-content-center mt-4">
                <div class="col">
                    <div class="d-flex justify-content-center">
                        <a href="#"><i class="fab fa-twitter fa-3x text-primary"></i></a>
                    </div>
                </div>
                <div class="col">
                    <div class="d-flex justify-content-center">
                        <a href="#"><i class="fab fa-instagram fa-3x"></i></a>
                    </div>
                </div>
                <div class="col">
                    <div class="d-flex justify-content-center">
                       <a href="#"><i class="fab fa-snapchat fa-3x text-warning"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

  </div>
</main>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</body>
</html>


<?php }
}
