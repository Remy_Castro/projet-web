<?php
/* Smarty version 3.1.34-dev-7, created on 2020-12-06 15:29:46
  from '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/candidature_form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fccf8eaa6e398_60201241',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eecfe9c3fc9ca697b1e779de2056f09d21154e00' => 
    array (
      0 => '/srv/disk12/3672955/www/festimusic.atwebpages.com/templates/candidature_form.tpl',
      1 => 1607268051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fccf8eaa6e398_60201241 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<main class="h-auto">
    <form method="POST" enctype="multipart/form-data">
        <div class="infos_groupe container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Informations du groupe</h1>
                <div class="row card-body">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['grp_name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                    <label for="" class="">Nom du groupe</label>
                                    <input type="text" class="form-control" name="group_name" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['group_name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                                </div>
                                <div class="col">
                                    <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['date'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                    <label for="creation_year" class="">Année de création</label>
                                    <select name="creation_year" id="creation_year" class="form-control">
                                        <option value='<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['creation_year'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
'><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['creation_year'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '- Année de création -' : $tmp);?>
</option>
                                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1900;
if ($_smarty_tpl->tpl_vars['i']->value <= date("Y")) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= date("Y"); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                                <?php echo $_smarty_tpl->tpl_vars['i']->value;?>

                                            </option>
                                        <?php }
}
?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['scene'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="type_scene">Type de scene</label>
                            <select name="type_scene" id="type_scene" class="form-control">
                                <option value='<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['type_scene'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
'><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['nom_scene']->value, ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '- Type de scene -' : $tmp);?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['scene']->value, 'ligne');
$_smarty_tpl->tpl_vars['ligne']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ligne']->value) {
$_smarty_tpl->tpl_vars['ligne']->do_else = false;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[1];?>
</option>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </select>
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['presentation'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="presentation">Présentation du groupe</label>
                            <textarea id="presentation" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="presentation"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['presentation'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</textarea>
                        </div>

                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['link_fb'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="site_fb">Site web ou page facebook</label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_webFB" id="site_fb" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['link_webFB'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['link_ytb'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="youtube">Adresse page youtube <span class="text-muted">(Facultatif)</span></label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_ytb" id="youtube" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['link_ytb'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['dpt'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="departement">Departement d'origine</label>
                            <select name="dpt" id="departement" class="form-control">
                                <option value='<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['dpt'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
'><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['dpt'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '- Département - ' : $tmp);?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['departement']->value, 'ligne');
$_smarty_tpl->tpl_vars['ligne']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ligne']->value) {
$_smarty_tpl->tpl_vars['ligne']->do_else = false;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
 - <?php echo $_smarty_tpl->tpl_vars['ligne']->value[1];?>
</option>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </select>
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['style_musical'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="">Style musical</label>
                            <input type="text" class="form-control" placeholder="Exemple : rock,punk..." name="style_music" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['style_music'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['experience'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="experience">Expérience scénique</label>
                            <textarea id="experience" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="experience"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['experience'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</textarea>
                        </div>
                        <div class="form-group">
                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['link_soundcloud'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                            <label for="soundcloud">Adresse page soundcloud <span class="text-muted">(Facultatif)</span></label>
                            <input type="url" placeholder="https://..." class="form-control" name="link_soundcloud" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['link_soundcloud'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="infos_perso container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Informations individuelles</h1>
                <div class="row card-body">
                    <div class="col">
                        <div class="répresentant">
                            <p class="lead">Répresentant du groupe</p>

                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-nom">Nom</label>
                                <input type="text" class="form-control" name="leader_name" id="l-nom" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_name'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>

                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_prenom'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-prenom">Prénom</label>
                                <input type="text" class="form-control" name="leader_prenom" id="l-prenom" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_prenom'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>
                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_adresse'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-adresse">Adresse</label>
                                <input type="text" class="form-control" name="leader_adress" id="l-adresse" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_adress'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>
                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_code_postal'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-cod">Code postal</label>
                                <input type="text" class="form-control" name="leader_codPost" id="l-cod" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_codPost'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>
                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_email'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-mail">Email</label>
                                <input type="text" class="form-control" name="leader_email" id="l-mail" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_email'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>
                            <div class="form-group">
                                <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lead_phone'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                <label for="l-phone">Téléphone</label>
                                <input type="text" class="form-control" name="leader_phone" placeholder="Exemple : 0683627199" id="l-phone" value="<?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['leader_phone'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
">
                            </div>
                        </div>
                        <div class="members-card">
                            <p class="lead mt-5 d-inline-block ml-5">Membre(s) du groupe  <span class="text-muted" style="font-size:16px">(les membres non confirmés ne sont pas enregistrés)</span></p>
                            <div class="group-btn d-flex ml-5">
                                <button type="button" class="btn add btn-success" onclick="add()">Ajouter un membre</button>
                                <button type="button" class="btn del btn-danger ml-2" onclick="del()">Supprimer un membre</button>
                            </div>
                            <div id="card-container" class="row justify-content-around">

                                <div class="card col-5 mt-3">
                                    <p class="text-center" style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['confirm_membre'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                    <div class="card-title text-center">Membre 1</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nom</label>
                                            <input type="text" class="last_name form-control" name="member_nom"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="">Prénom</label>
                                            <input type="text" class="first_name form-control" name="member_prenom"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="">Instruments</label>
                                            <input type="text" class="instru form-control" name="member_instruments"  >
                                        </div>
                                        <div class="send_info">
                                            <button type="button" class="btn btn-secondary mx-auto d-block"  onclick="confirm_aMember()"  >Confirmer ce membre</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="info_membres" id="members_jsToHtml" class="form-control" style="display: none;">

                            </div>
                            <div class="row">
                                <div class="col-12 mx-auto mt-5">
                                    <div class="d-flex flex-row">
                                        <div class="form-check">
                                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['statut_asso'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                            <p>Avez-vous un statut associatif ?  </p>
                                            <input type="radio" id="asso_yes" name="statut_asso" value="o">
                                            <label for="asso_yes">Oui</label>
                                            <input type="radio" id="asso_no" name="statut_asso" class="ml-2" value="n">
                                            <label for="asso_no">Non</label>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row mt-3">
                                        <div class="form-check">
                                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['sacem'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                            <p>Êtes-vous inscrit à la SACEM ?</p>
                                            <input type="radio" id="sacem_yes" name="inscrit_sacem" class="ml-1" value="o">
                                            <label for="sacem_yes">Oui</label>
                                            <input type="radio" id="sacem_no" name="inscrit_sacem" class="ml-2" value="n">
                                            <label for="sacem_no">Non</label>

                                        </div>
                                    </div>
                                    <div class="d-flex flex-row mt-3">
                                        <div class="form-check">
                                            <p style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['producteur'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</p>
                                            <p>Êtes-vous producteur ?</p>
                                            <input type="radio" id="product_yes" name="is_producteur" class="ml-2" value="o">
                                            <label for="product_yes">Oui</label>
                                            <input type="radio" id="product_no" name="is_producteur" class="ml-2" value="n">
                                            <label for="product_no">Non</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="pieces-jointes container mt-5">
            <div class="card border-dark">
                <h1 class="card-title bg-dark py-4 text-white text-center">Pièces à fournir</h1>
                <div class="row card-body">
                    <div class="col-12">
                        <div class="file_music">
                            <p>- Insérez trois de vos musiques <span class="text-muted">(MP3)</span></p>
                            <div class="form-group">
                                <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['music1'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                <label for="music1">Musique 1 : </label>
                                <input type="file" name="fileMusic_1" id="music1">
                            </div>
                            <div class="form-group">
                                <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['music2'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                <label for="music2">Musique 2 : </label>
                                <input type="file" name="fileMusic_2" id="music2">
                            </div>
                            <div class="form-group">
                                <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['music3'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                <label for="music3">Musique 3 : </label>
                                <input type="file" name="fileMusic_3" id="music3">
                            </div>
                        </div>

                        <div class="picture">
                            <p>- Deux photos de groupe : <span class="text-muted">(JPEG - PNG, Résolution supérieure à 300 dpi)</span></p>
                            <div class="form-group">
                                <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['img1'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                <label for="img1">Photo 1 : </label>
                                <input type="file" name="img-1" id="img1">
                            </div>

                            <div class="form-group">
                                <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['img2'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                <label for="img2">Photo 2 : </label>
                                <input type="file" name="img-2" id="img2">
                            </div>
                        </div>
                        <div class="form-group">
                            <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['f_tech'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                            <label for="f_technique">Fiche technique <span class="text-muted">(PDF)</span></label>
                            <br>
                            <input type="file" id="f_techique" name="f_tech">
                        </div>
                        <div class="form-group">
                            <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['d_presse'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                            <label for="presse">- Dossier de presse <span class="text-muted">(PDF)</span></label>
                            <br>
                            <input type="file" id="presse" name="dossier_presse">
                        </div>
                        <div class="sacem_pdf mt-5">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <span style="color:red"><?php echo (($tmp = @htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['setlist'], ENT_QUOTES, 'UTF-8', true))===null||$tmp==='' ? '' : $tmp);?>
</span>
                                        <label for="sacem_pdf">- Document SACEM / SETLIST<span class="text-muted">(PDF)</span></label>
                                        <input type="file" id="sacem_pdf" name="setlist" class="ml-2">
                                    <!-- Afficher un input de type file -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary d-block mx-auto mt-5 w-50 form-control">Envoyez sa candidature</button>
        </div>
    </form>
</main>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
<?php echo '<script'; ?>
 src="../script/add_memberCard.js"><?php echo '</script'; ?>
>
</html>
<?php }
}
