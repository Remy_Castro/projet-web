<?php
/* Smarty version 3.1.34-dev-7, created on 2020-11-22 09:22:50
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5fba2dea5b1d34_11455631',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5970d2e7fbdef75af1c6874752a4f0aa97326bc1' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature2.tpl',
      1 => 1606036918,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5fba2dea5b1d34_11455631 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <?php echo '<script'; ?>
 src="https://kit.fontawesome.com/7a68f7bd78.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="../style/style.css" type="text/css">


</head>
<body>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('watchCandidature'=>$_smarty_tpl->tpl_vars['isAdmin']->value), 0, false);
?>
    <main>
        <form method="POST">
            <div class="infos_groupe container mt-5">
                <div class="card border-dark">
                    <h1 class="card-title bg-dark py-4 text-white text-center">Informations du groupe</h1>
                    <div class="row card-body">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="">Nom du groupe</label>
                                        <input type="text" class="form-control" name="group_name">
                                    </div>
                                    <div class="col">
                                        <label for="creation_year" class="">Année de création</label>
                                        <select name="creation_year" id="creation_year" class="form-control" required>
                                            <option value=''>- Année de création -</option>
                                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1900;
if ($_smarty_tpl->tpl_vars['i']->value <= date("Y")) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= date("Y"); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['i']->value;?>

                                                </option>
                                            <?php }
}
?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type_scene">Type de scene</label>
                                <select name="type_scene" id="type_scene" class="form-control">
                                    <option value=''>- Type de scene -</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['scene']->value, 'ligne');
$_smarty_tpl->tpl_vars['ligne']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ligne']->value) {
$_smarty_tpl->tpl_vars['ligne']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[1];?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="presentation">Présentation du groupe</label>
                                <textarea id="presentation" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="presentation"></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label for="site_fb">Site web ou page facebook</label>
                                <input type="url" placeholder="https://..." class="form-control" name="link_webFB" id="site_fb">
                            </div>
                            <div class="form-group">
                                <label for="youtube">Adresse page youtube</label>
                                <input type="url" placeholder="https://..." class="form-control" name="link_ytb" id="youtube">
                            </div>
                            
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="departement">Departement d'origine</label>
                                <select name="dpt" id="departement" class="form-control">
                                    <option value=''>- Département - </option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['departement']->value, 'ligne');
$_smarty_tpl->tpl_vars['ligne']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ligne']->value) {
$_smarty_tpl->tpl_vars['ligne']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
 - <?php echo $_smarty_tpl->tpl_vars['ligne']->value[1];?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Style musical</label>
                                <input type="text" class="form-control" placeholder="Exemple : rock,punk..." name="style_music">
                            </div>
                            <div class="form-group">
                                <label for="">Expérience scénique</label>
                                <textarea name="" id="" cols="30" rows="10" maxlength="500" class="form-control" placeholder="500 caractères max" name="experience"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="soundcloud">Adresse page soundcloud</label>
                                <input type="url" placeholder="https://..." class="form-control" name="link_souncloud" name="soundcloud">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="infos_perso container mt-5">
                <div class="card border-dark">
                    <h1 class="card-title bg-dark py-4 text-white text-center">Informations individuelles</h1>
                    <div class="row card-body">
                        <div class="col">
                            <div class="répresentant">
                                <p class="lead">Répresentant du groupe</p>
                                
                                <div class="form-group">
                                    <label for="">Nom</label>
                                    <input type="text" class="form-control" name="leader_name">
                                </div>
                        
                                <div class="form-group">
                                    <label for="">Prénom</label>
                                    <input type="text" class="form-control" name="leader_prenom">
                                </div>
                                <div class="form-group">
                                    <label for="">Adresse</label>
                                    <input type="text" class="form-control" name="leader_adress">
                                </div>
                                <div class="form-group">
                                    <label for="">Code postal</label>
                                    <input type="text" class="form-control" name="leader_codPost">
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control" name="leader_email">
                                </div>
                                <div class="form-group">
                                    <label for="">Téléphone</label>
                                    <input type="text" class="form-control" name="leader_phone" placeholder="Exemple : 0683627199">
                                </div>
                            </div>
                            <div class="members-card">
                                <p class="lead mt-5 d-inline-block ml-5">Membre(s) du groupe</p>
                                <div class="group-btn d-flex ml-5">
                                    <button type="button" class="btn add btn-success" onclick="add()">Ajouter un membre</button>
                                    <button type="button" class="btn del btn-danger ml-2" onclick="del()">Supprimer un membre</button>
                                </div>
                                <div id="card-container" class="row justify-content-around">
                                    <div class="card col-5 mt-3">
                                        <div class="card-title text-center">Membre 1</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nom</label>
                                                <input type="text" class="last_name form-control" name="member1_nom" required>
                                            </div>    
                                            <div class="form-group">
                                                <label for="">Prénom</label>
                                                <input type="text" class="first_name form-control" name="member1_prenom" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Instruments</label>
                                                <input type="text" class="instru form-control" name="member1_instruments" required>
                                            </div>
                                            <div class="send_info">
                                                <button type="button" class="btn btn-secondary mx-auto d-block"  onclick="confirm_aMember()">Confirmer ce membre</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <input type="text" name="info_membres" id="members_jsToHtml" class="form-control" style="display: none;">

                             </div>
                                <div class="row">
                                    <div class="col-12 mx-auto mt-5">
                                        <div class="d-flex flex-row">
                                            <p>Avez-vous un statut associatif ?  </p>
                                                <div class="form-check">
                                                    <input type="radio" id="asso_yes" name="statut_asso">
                                                    <label for="asso_yes">Oui</label>
                                                    <input type="radio" id="asso_no" name="statut_asso" class="ml-2">
                                                    <label for="asso_no">Non</label>
                                                </div>            
                                        </div>
                                        <div class="d-flex flex-row mt-3">
                                            <p>Êtes-vous inscrit à la SACEM ?</p>
                                            <div class="form-check">
                                                <input type="radio" id="sacem_yes" name="inscrit_sacem" class="ml-1">
                                                <label for="sacem_yes">Oui</label>
                                                <input type="radio" id="sacem_no" name="inscrit_sacem" class="ml-2">
                                                <label for="sacem_no">Non</label>
                                            </div>   
                                        </div>
                                        <div class="d-flex flex-row mt-3">
                                            <p>Êtes-vous producteur ?</p>
                                            <div class="form-check ml-5">
                                                <input type="radio" id="product_yes" name="is_producteur" class="ml-2">
                                                <label for="product_yes">Oui</label>            
                                                <input type="radio" id="product_no" name="is_producteur" class="ml-2">
                                                <label for="product_no">Non</label>
                                            </div>      
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>

            <div class="pieces-jointes container mt-5">
                <div class="card border-dark">
                    <h1 class="card-title bg-dark py-4 text-white text-center">Pièces à fournir</h1>
                    <div class="row card-body">
                        <div class="col-12">
                            <div class="file_music">
                                <p>- Insérez trois de vos musiques</p>
                                <div class="form-group">
                                    <label for="">Musique 1 : </label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label for="">Musique 2 : </label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label for="">Musique 3 : </label>
                                    <input type="file">
                                </div>
                            </div>
                            
                            <div class="picture">
                                <p>- Deux photos de groupe : </p>
                                <div class="form-group">
                                    <label for="">Photo 1 : </label>
                                    <input type="file">
                                </div>
        
                                <div class="form-group">
                                    <label for="">Photo 2 : </label>
                                    <input type="file">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Fiche technique (PDF)</label>
                                <br>
                                <input type="file">
                            </div>
                            <div class="form-group">
                                <label for="">- Dossier de presse (PDF)</label>
                                <br>
                                <input type="file">
                            </div>
                            <div class="sacem_pdf mt-5">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="sacem_pdf">- Document SACEM (PDF)</label>
                                            <input type="radio" id="sacem_pdf" name="setlist" class="ml-2">
                                        <!-- Afficher un input de type file -->
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="info_musiques">Autre</label>
                                            <input type="radio" id="info_musiques" name="setlist" class="ml-2">
                                            <!-- Afficher inputs : noms, compositeurs, durée morceaux setlist = liste musiques jouées -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary d-block mx-auto mt-5 w-50 form-control">Envoyez sa candidature</button>
            </div>
        </form>
    </main>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('watchCandidature'=>$_smarty_tpl->tpl_vars['isAdmin']->value), 0, false);
?>
</body>
<?php echo '<script'; ?>
 src="../script/add_memberCard.js"><?php echo '</script'; ?>
>
</html><?php }
}
