<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 17:49:42
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_pasAccess.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff34726c6f6a0_44852021',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '51c738e2ef34b5feaf241fa754155fdeb82e7c6b' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_pasAccess.tpl',
      1 => 1609778891,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff34726c6f6a0_44852021 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span>Vous n'avez pas accès à cette candidature.</span></h1>
        </div>
    </div>

</div>
<style>
    @media screen and (max-width: 500px){
        h1{
            font-size: 25px;
        }
    }
</style>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
