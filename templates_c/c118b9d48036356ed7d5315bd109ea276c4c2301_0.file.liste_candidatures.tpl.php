<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 18:28:55
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/liste_candidatures.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff350579bf7a6_69663592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c118b9d48036356ed7d5315bd109ea276c4c2301' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/liste_candidatures.tpl',
      1 => 1609781334,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff350579bf7a6_69663592 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container center mt-5 pb-3 w-100">
    <div class="card">
        <div class="card-header p-3">

            <h1 class="text-center">Toutes les candidatures</h1>
        </div>
        <card class="body d-flex mt-5 pb-5 overflow-auto">
            <table class="w-75">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center"></th>
                    <th class="text-center">Nom groupe</th>
                    <th class="text-center">Departement</th>
                    <th class="text-center">Type de scène</th>
                    <th class="text-center">Style</th>
                    <th class="text-center">Année de création</th>
                    <th class="text-center">Présentation</th>
                    <th class="text-center">Expérience</th>
                    <th class="text-center">Nom du représentant</th>
                    <th class="text-center">Prénom du représentant</th>
                    <th class="text-center">Adresse du représentant</th>
                    <th class="text-center">Code postal du représentant</th>
                    <th class="text-center">Email du représentant</th>
                    <th class="text-center">Téléphone du représentant</th>
                    <th class="text-center">Membres du groupe</th>
                    <th class="text-center">Youtube</th>
                    <th class="text-center">Soundcloud</th>
                    <th class="text-center">Site ou page facebook</th>
                    <th class="text-center">Producteur </th>
                    <th class="text-center">Statut associatif</th>
                    <th class="text-center">Inscrit à la sacem</th>
                    <th class="text-center">Nom compte</th>
                    <th class="text-center">Musique 1 </th>
                    <th class="text-center">Musique 2 </th>
                    <th class="text-center">Musique 3 </th>
                    <th class="text-center">Photo 1 </th>
                    <th class="text-center">Photo 2 </th>
                    <th class="text-center">Fiche technique </th>
                    <th class="text-center">Dossier presse </th>
                    <th class="text-center">Setlist/Sacem </th>
                </tr>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste']->value, 'ligne', false, 'key');
$_smarty_tpl->tpl_vars['ligne']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['ligne']->value) {
$_smarty_tpl->tpl_vars['ligne']->do_else = false;
?>
                    <tr>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[0];?>
</td>
                        <td class="text-center"><a href="/detail/<?php echo $_smarty_tpl->tpl_vars['ligne']->value[1];?>
" class="text-primary">Détail</a></td>
                                                <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 21+1 - (1) : 1-(21)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration === 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration === $_smarty_tpl->tpl_vars['i']->total;?>
                            <td class="text-center"><p><?php echo $_smarty_tpl->tpl_vars['ligne']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</p></td>
                        <?php }
}
?>
                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 29+1 - (22) : 22-(29)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 22, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration === 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration === $_smarty_tpl->tpl_vars['i']->total;?>
                            <td><a href="../data/uploads/<?php echo $_smarty_tpl->tpl_vars['ligne']->value[$_smarty_tpl->tpl_vars['i']->value];?>
" class="text-primary"><?php echo $_smarty_tpl->tpl_vars['ligne']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</a></td>
                        <?php }
}
?>
                    <tr>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


            </table>
        </card>
    </div>
</div>
<style>
    p{
        max-height: 100px;
        overflow: scroll;
    }
</style>

<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
