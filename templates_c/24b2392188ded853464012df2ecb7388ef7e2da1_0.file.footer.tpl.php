<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 16:55:05
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff33a59de52d9_78975785',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24b2392188ded853464012df2ecb7388ef7e2da1' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/footer.tpl',
      1 => 1609775704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ff33a59de52d9_78975785 (Smarty_Internal_Template $_smarty_tpl) {
?><footer class="border border-dark pb-3 pb-sm-3 pb-md-0 pb-lg-3 bg-dark">
        <div class="container">
            <div class="row justify-content-center mt-3">
                <h3><a href="/" class="text-white"><span class="text-danger">Festi</span>'<span class="text-primary">Music</span></a></h3>
            </div>
            <div class="row justify-content-center mt-3">
                <div class=" col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white text-center">Accueil</a>
                </div>
                <div class="col-sm-2 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="/" class="text-white">Partenaires</a>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4 text-center text-white menu">
                    <a href="candidature" class="text-white">Déposer sa candidature</a>
                </div>
            </div>
        </div>
  </footer>
  <style>
  @media screen and (max-width: 450px){
        footer .row{
            display:flex;
            justify-content : center;
            flex-direction:column;
            margin-right: 0 !important;
        }
        footer .menu{
            width: 100% !important;
            max-width: 100% !important;
            margin-top: 5px;
        }
        footer h3{
            text-align : center !important;
        }
    }
</style>
<?php }
}
