<?php
/* Smarty version 3.1.34-dev-7, created on 2021-01-04 15:26:30
  from '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_doublon.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ff32596b946c0_98642639',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '36fa0862bcb34829cfa112c9fecc1788155593c4' => 
    array (
      0 => '/Users/remycastro/Documents/Informatique/Cours/A2/Projets/projet-web/templates/candidature_doublon.tpl',
      1 => 1609763979,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5ff32596b946c0_98642639 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="container w-100 my-auto">
    <div class="card p-5">
        <div class="card-body">
            <h1 class="card-title text-center p-5"><span><?php echo $_smarty_tpl->tpl_vars['source']->value;?>
</span></h1>
            <?php if ($_smarty_tpl->tpl_vars['source_nomCompte']->value == true) {?>
                <a href="/connexion" class="btn btn-primary mx-auto d-block">Voir ma candidature</a>
            <?php }?>
        </div>

    </div>
</div>
<style>
    h1{
        font-size: 25px;
    }
</style>
<?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
