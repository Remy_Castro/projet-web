const MAX_MEMBERS = 8;
const container = document.getElementById("card-container");
const add_btn = document.getElementById("add");
const del_btn = document.getElementById("del");
var cards = document.getElementsByClassName('card');
const input_convert = document.getElementById('members_jsToHtml');

var counter = 1;
var nb_validated = 0;
var members_data = [];


function add(){
    if (counter < MAX_MEMBERS){
        if(counter == nb_validated){
            counter++;
        container.insertAdjacentHTML('beforeend', '<div class="card col-5 mt-3 member-card">' +
        '<div class="card-title text-center mt-3">Membre '+ counter +'</div>' +
        '<div class="card-body">' +
            '<div class="form-group">'+
                '<label for="">Nom</label>'+
                '<input type="text" class="last_name form-control" required>'+
            '</div>'+
            '<div class="form-group">'+
                '<label for="">Prénom</label>'+
                '<input type="text" class="first_name form-control" name="member2_prenom" required>'+
            '</div>'+
            '<div class="form-group">'+
                '<label for="">Instruments</label>'+
                '<input type="text" class="instru form-control" name="member2_instruments" required>'+
            '</div>'+
            '<div class="send_info">'+
                '<button type="button" class="btn btn-secondary mx-auto d-block" onclick="confirm_aMember()">Confirmer ce membre</button>'+
            '</div>'+
        '</div>'+
    '</div>')
        }else{
            alert("Vous devez valider tous vos membres avant d'en ajouter");
        }
    } else{
        alert("Vous ne pouvez pas dépasser 8 membres.")
    }
}
function del(){
    if(counter > 1){
        if(counter == nb_validated){ //si pas ce if : même si on supprime 1 card pas validée : affiche l'erreur
            members_data.pop(); //on supprime le dernier élément du tableau
            input_convert.value = members_data;

            nb_validated--;
        }
        counter--;
        container.removeChild(container.lastElementChild);

    } else{
        alert("Il faut au minimum 1 membre.");
    }

}

function confirm_aMember(){
    let error = false;
    var inputs = container.lastElementChild.querySelectorAll('input');
    var confirm_button = container.lastElementChild.lastElementChild.querySelector('button');
    //on regarde si les champs sont vides ou non en parcourant tous les inputs:
    for(let i = 0; i < inputs.length; i++){
        if(!(inputs[i].value).replace(/\s+/, '').length){
            error = true; // si il est vide -> erreur
        }
    }
    //on parcours une deuxième fois pour les désactiver
    for(let i = 0; i < inputs.length; i++){
        if(!error){
            inputs[i].setAttribute('disabled','true');
        }
    }
    //Affichage si l'utilisateur valide le membre avec au moins 1 champ vide
    if(error){
        alert('Vous devez remplir tous les champs des membres.');
    }else{
        nb_validated++;
        confirm_button.setAttribute('disabled','true');
        members_data.push(recoverInfo(container.lastElementChild));

        input_convert.value = members_data;
    }
}

function recoverInfo(element){
    var last_name = element.querySelector('.last_name').value;
    var first_name = element.querySelector('.first_name').value;
    var instru = element.querySelector('.instru').value;
    return last_name + '-' + first_name + '-' + instru;
}
