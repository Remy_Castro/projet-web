<?php

$dbServer = '127.0.0.1';
$dbName ='projet_web_durand';
$dbUsername = 'root';
$dbPassword = 'root';

$db = new PDO("mysql:host=$dbServer;dbname=$dbName;charset=utf8",
    $dbUsername,
    $dbPassword
);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


?>
