<?php
Flight::set('db', $db); //$db initialisé dans fichier pdo qui est inclu dans index.php


//liens header
//Si l'utilisateur n'est pas connecté :
if (!isset($_SESSION['username'])) {
    $link = "<a class='nav-link' href='/inscription'>S'inscrire</a>";
    $link_2 = "<a class='nav-link' href='/connexion'>Se connecter</a>";
} else {//utilisateur connecté :
    $link = "<a class='nav-link' href='/profil'>Mon profil</a>";
    $link_2= "<a class='nav-link' href='/deconnexion'>Me déconnecter</a>";
}
$links_header = array(
"link" => $link,
"link_2"=>$link_2,
);
Flight::view()->assign('data',$links_header);


//On regarde si l'utilisateur est un responsable :
if(isset($_SESSION['username'])){
    if($_SESSION['responsable'][0] == 'o'){ //On récupère la lettre stockée dans le tableau $_SESSION['responsable']
        $link_3 = "<a class='nav-link' href='/liste'>Voir les candidatures</a>";
        Flight::view()->assign('lien_responsable',$link_3);
    }
}



Flight::route('/', function () {
    Flight::view()->display('./templates/index.tpl');
});

Flight::route('GET /inscription', function(){

    Flight::view()->assign('titre','Inscription'); //Permet de définir un titre pour chaque page
    Flight::view()->display('./templates/inscription.tpl');
});
Flight::route('POST /inscription', function(){
    Flight::view()->assign('titre','Inscription');

    $messages=array();

  //test nom
  if (empty(Flight::request()->data->name)) {
      $messages['error_nom'] = "Le nom est obligatoire";
  }
  //test mail
  if (empty(Flight::request()->data->email)) {
      $messages['error_email']="L'email est obligatoire";
  } else if (filter_var(Flight::request()->data->email, FILTER_VALIDATE_EMAIL) === FALSE) {
      $messages['error_email']= "L'email est invalide";
  } else {
      $db = Flight::get('db');
      $requete_searchEmail = $db->prepare("SELECT COUNT(*) FROM utilisateur WHERE email = :email ");
      $requete_searchEmail->execute(array(
          ":email"=>Flight::request()->data->email
      ));
      $isEmailExist = $requete_searchEmail->fetch();
      if($isEmailExist[0] == 1){ //adresse mail existe déjà
          $messages['error_email']= "Email déjà utilisée";
      }
  }

  //test mot de passe
  if (empty(Flight::request()->data->password) || strlen(Flight::request()->data->password) < 8){
      $messages['error_password']="La longueur du mot de passe doit être supérieure à 8";
  }

  if(empty($messages)){ //Si pas d'erreur
      $db = Flight::get('db');
      $create_user = $db->prepare('INSERT INTO utilisateur VALUES(:name,:email,:password, "n")');
      $create_user->execute(array(
          ":name"=>Flight::request()->data->name,
          ":email"=>Flight::request()->data->email,
          ":password"=>password_hash(Flight::request()->data->password, PASSWORD_DEFAULT),
      ));
      Flight::view()->display("./templates/success.tpl");
  } else {
      Flight::view()->assign("messages",$messages);
      $data = array(
          "messages"=>$messages,
          "post"=>$_POST
      );

      Flight::render("./templates/inscription.tpl", $data);
  }
});

Flight::route('GET /connexion', function(){
    Flight::view()->assign('titre','Connexion');

    Flight::view()->display('./templates/connexion.tpl');
});
Flight::route('POST /connexion', function(){
    Flight::view()->assign('titre','Connexion');

    $messages=array();
    $erreur = false;

    Flight::view()->assign('title','Connexion');

    if(empty(Flight::request()->data->name)){
        $messages['nom'] = 'Veuillez entrer un nom.';
        $erreur = true;
    }
    if(empty(Flight::request()->data->email)){
        $messages['email'] = 'Veuillez entrer une adresse mail.';
        $erreur = true;
    }
    if(empty(Flight::request()->data->password)){
        $messages['password'] = 'Veuillez entrer un mot de passe.';
        $erreur = true;
    }
    //Si aucun des champs sont vides : on vérifie existance mail
    if($erreur == false){
        $db = Flight::get('db');
        $requete_searchEmail = $db->prepare("SELECT COUNT(*) FROM utilisateur WHERE email = :email ");
        $requete_searchEmail->execute(array(
            ":email" => Flight::request()->data->email
        ));
        $isEmailExist = $requete_searchEmail->fetch();
        if ($isEmailExist[0] == 0) { //adresse mail n'existe pas
            $erreur = true;
            $messages['no_email'] = "L'adresse mail n'existe pas.";
        }
    }
    //Si les saisies sont correctes : on vérifie si il existe une ligne ou le duo nom/email saisie existe
    //Si oui, on vérifie que le mot de passe récupéré correspond avec celui de la bdd
    if (!$erreur) {
        $db = Flight::get('db');
        $requete_isDuoCorrect = $db->prepare("SELECT COUNT(*) from  utilisateur WHERE nom = :nom AND email = :email");
        $requete_isDuoCorrect->execute(array(
            ":nom" => Flight::request()->data->name,
            ":email" => Flight::request()->data->email
        ));
        $isDuoExist = $requete_isDuoCorrect->fetch();

        if ($isDuoExist[0] == 0) {
            $messages['duo'] = "Nom d'utilisateur incorrect";
         } else { //On va chercher le bon mot de passe
            $requete_searchPass = $db->prepare("SELECT motdepasse from utilisateur where nom = :nom AND email = :email");
            $requete_searchPass->execute(array(
                ":nom" => Flight::request()->data->name,
                ":email" => Flight::request()->data->email
            ));
            $good_pass = $requete_searchPass->fetch();
            if (!(password_verify(Flight::request()->data->password, $good_pass[0]))) {
                $messages['mdp'] = "Mot de passe incorrect.";
             }
        }
    }
    if(empty($messages)){
        $db = Flight::get('db');
        $requete_role = $db->prepare('select responsable from utilisateur where email = :email');
        $requete_role->execute(array(
            ":email"=>Flight::request()->data->email
        ));
        $q = $requete_role->fetch();
        $_SESSION['username'] = htmlentities(Flight::request()->data->name);
        $_SESSION['responsable'] = $q; //on stocke la responsabilité de l'utilisateur

        Flight::redirect('/');
    }else{
        $data = array(
            "messages"=>$messages,
            "post"=>$_POST
        );
        Flight::render("./templates/connexion.tpl", $data);
    }


});


Flight::route('GET /candidature', function() {
    Flight::view()->assign('titre','Candidature');

    if(isset($_SESSION['username'])){
        $db = Flight::get('db');
        $requete_departement = $db->query('select * from departement order by departement_code');
        $requete_scene = $db->query('select * from scene');
        $ligne_scene = $requete_scene->fetchAll();
        $ligne_dept = $requete_departement->fetchAll();

        $data = array(
            "scene"=>$ligne_scene,
            "departement"=>$ligne_dept,
        );

        Flight::render("./templates/candidature_form.tpl",$data);
    }else{
        Flight::redirect('/connexion');
    }
});
Flight::route('POST /candidature', function() {
    Flight::view()->assign('titre','Candidature');

    $db = Flight::get('db');
    $requete_departement = $db->query('select * from departement order by departement_code');
    $requete_scene = $db->query('select * from scene');

    $ligne_scene = $requete_scene->fetchAll();
    $ligne_dept = $requete_departement->fetchALl();

    // **** VÉRIFICATION PARTIE 1 : Les informations concernants le groupe ****
    $message = array();
    //verification champ nom de groupe
    if(empty(Flight::request()->data->group_name)){
        $message['grp_name'] = "Entrez un nom de groupe";
    }
    //verification année
    if(empty(Flight::request()->data->creation_year)){
        $message['date'] = "Saisir une date";
    }
    //vérification type de scene
    if(empty(Flight::request()->data->type_scene)){
        $message['scene'] = "Entrez un type de scene";
    }
    //vérification présentation
    if(empty(Flight::request()->data->presentation)){
        $message['presentation'] = 'Remplir ce champ';
    }
    //Vérification expérience scénique
    if(empty(Flight::request()->data->experience)){
        $message['experience'] = 'Remplir ce champ';
    }
    //Vérification site ou page facebook
    if(empty(Flight::request()->data->link_webFB)){
        $message['link_fb'] = 'Remplir ce champ';
    } else if(!filter_var(Flight::request()->data->link_webFB, FILTER_VALIDATE_URL)){
        $message['link_fb'] = "Format du lien invalide";
    }
    //Vérification lien youtube (s'il exsite car facultatif)
    if(!empty(Flight::request()->data->link_ytb)){
        if(!filter_var(Flight::request()->data->link_ytb, FILTER_VALIDATE_URL)){
            $message['link_ytb'] = "Format du lien invalide";
        }
    }
    //Vérification département
    if(empty(Flight::request()->data->dpt)){
        $message['dpt'] = "Veuillez entrer un département";
    }
    //Vérification style musical
    if(empty(Flight::request()->data->style_music)){
        $message['style_musical'] = "Remplir ce champ";
     }
     //Vérification lien soundcloud (s'il exsite car facultatif)
     if(!empty(Flight::request()->data->link_soundcloud)){
        if(!filter_var(Flight::request()->data->link_soundcloud, FILTER_VALIDATE_URL)){
            $message['link_soundcloud'] = "Format du lien invalide";
        }
    }


  // ****VÉRIFICATION PARTIE 2 : Informations personnelles****

    //Vérification du nom de représetant
    if(empty(Flight::request()->data->leader_name)){
        $message['lead_name'] = "Entrez un nom de groupe";
    }
    //Vérification du prénom du représentat
    if(empty(Flight::request()->data->leader_prenom)){
        $message['lead_prenom'] = "Entrez un prénom";
    }
    //Vérification de l'adresse du représentant
    if(empty(Flight::request()->data->leader_adress)){
        $message['lead_adresse'] = "Entrez une adresse";
    }
    //Vérification du code postal
    if(empty(Flight::request()->data->leader_codPost)){
        $message['lead_code_postal'] = "Entrez un code postal";
    } else if(!preg_match('#^[0-9]{5}$#', Flight::request()->data->leader_codPost)){
        $message['lead_code_postal'] = "Vous devez rentrer  un code postal valide";
    }
    //Vérification de l'adresse mail du représentant
    if(empty(Flight::request()->data->leader_email)){
        $message['lead_email'] = "Entrez une adresse mail";
    } else if(!filter_var(Flight::request()->data->leader_email, FILTER_VALIDATE_EMAIL)){
        $message['lead_email'] = "Entrez un adresse mail valide";
    }
    //Vérification du téléphone du représentant
    if(empty(Flight::request()->data->leader_phone)){
        $message['lead_phone'] = "Entrez un numéro de téléphone";

        //doit commencer par un 0, puis une valeur de 1 à 9. Ensuite, contient quatre couples de 1 à 9.
    }else if(!preg_match('#[0][1-9][- \.?]?([0-9][0-9][- \.?]?){4}$#',Flight::request()->data->leader_phone)){
        $message['lead_phone'] = "Entrez un numéro de téléphone valide";
    }

    //On vérifie si l'utilisateur a confirmé un membre : pas utile de vérifier si les champs sont remplis car fait en JS
    if(empty(Flight::request()->data->info_membres)){
        $message['confirm_membre'] = "Vous devez confirmer ce membre";
    }
    //Vérification statut associatif
    if(empty(Flight::request()->data->statut_asso)){
        $message['statut_asso'] = 'Veuillez remplir ce champ';
    }
    if(empty(Flight::request()->data->inscrit_sacem)){
        $message['sacem'] = 'Veuillez remplir ce champ';
    }
    if(empty(Flight::request()->data->is_producteur)){
        $message['producteur'] = 'Veuillez remplir ce champ';
    }

  // **** VÉRIFICATIONS PARTIE 3 : Les pièces jointes ****

  //musique :
    //fichier musique 1 :
    if(isset(Flight::request()->files->fileMusic_1)){
        if(Flight::request()->files->fileMusic_1['error']!=0){
            $message['music1'] = "Fichier invalide";
        }else if(Flight::request()->files->fileMusic_1['type'] != 'audio/mpeg'){
            $message['music1'] = "Entrez un fichier audio de format MP3";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->fileMusic_1['size'] > 2097152){ //limite taille 8388608 bytes
            $message['music1'] = "Taille maximale du fichier : 2MO.";
        }
    }
    //fichier musique 2 :
    if(isset(Flight::request()->files->fileMusic_2)){
        if(Flight::request()->files->fileMusic_2['error']!=0){
            $message['music2'] = "Fichier invalide";
        }else if(Flight::request()->files->fileMusic_2['type'] != 'audio/mpeg'){
            $message['music2'] = "Entrez un fichier audio de format MP3";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->fileMusic_2['size'] > 2097152){
            $message['music2'] = "Taille maximale du fichier : 2MO.";
        }
    }
    //fichiers musique 3 :
    if(isset(Flight::request()->files->fileMusic_3)){
        if(Flight::request()->files->fileMusic_3['error']!=0){
            $message['music3'] = "Fichier invalide";
        }else if(Flight::request()->files->fileMusic_3['type'] != 'audio/mpeg'){
            $message['music3'] = "Entrez un fichier audio de format MP3";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->fileMusic_3['size'] > 2097152){
            $message['music3'] = "Taille maximale du fichier : 2MO.";
        }
    }
// images :

    //type image accepté : png,jpeg
    //fichiers photos
    if(isset(Flight::request()->files->img_1)){
        if(Flight::request()->files->img_1['error']!=0){
            $message['img1'] = "Fichier invalide";
        }else if((Flight::request()->files->img_1['type'] != 'image/jpeg')&&(Flight::request()->files->img_1['type'] != 'image/png')){
            $message['img1'] = "Formats acceptés : JPEG,PNG";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->img_1['size'] > 2097152){
            $message['img_1'] = "Taille maximale du fichier : 2MO.";

        }
    }

    if(isset(Flight::request()->files->img_2)){
        if(Flight::request()->files->img_2['error']!=0){
            $message['img2'] = "Fichier invalide";
        }else if((Flight::request()->files->img_2['type'] != 'image/jpeg')&&(Flight::request()->files->img_2['type'] != 'image/png')){
            $message['img2'] = "Formats acceptés : JPEG,PNG";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->img_2['size'] > 2097152){
            $message['img_2'] = "Taille maximale du fichier : 2MO.";

        }
    }

// pdf :
    //fichier fiche technique
    if(isset(Flight::request()->files->f_tech)){
        if(Flight::request()->files->f_tech['error'] != 0){
            $message['f_tech'] = "Fichier invalide";
        }else if(Flight::request()->files->f_tech['type'] != 'application/pdf'){
            $message['f_tech'] = 'Insérez le document au format PDF';
        }
        // Vérification taille du fichier
        if(Flight::request()->files->f_tech['size'] > 2097152){
            $message['f_tech'] = "Taille maximale du fichier : 2MO.";

        }
    }
    //fichier dossier de presse
    if(isset(Flight::request()->files->dossier_presse)){
        if(Flight::request()->files->dossier_presse['error'] != 0){
            $message['d_presse'] = "Fichier invalide";
        } else if (Flight::request()->files->dossier_presse['type'] != 'application/pdf'){
            $message['d_presse'] = "Insérez le document au format PDF";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->dossier_presse['size'] > 2097152){
            $message['d_presse'] = "Taille maximale du fichier : 2MO.";

        }
    }

    //fichier sacem
    if(isset(Flight::request()->files->setlist)){
        if(Flight::request()->files->setlist['error'] != 0 ){
            $message['setlist'] = "Fichier invalide";
        }else if(Flight::request()->files->dossier_presse['type'] != 'application/pdf'){
            $message['setlist'] = "Insérez le document au format PDF";
        }
        // Vérification taille du fichier
        if(Flight::request()->files->setlist['size'] > 2097152){
            $message['setlist'] = "Taille maximale du fichier : 2MO.";

        }
    }


    if(empty($message)) {
        //Vérification du doublon de candidature :
            // doublon candidature déjà entregistrée avec le compte
        $nom_utilisateur = $_SESSION['username'];
        $requete_doublonCandidature_byUsername = $db->prepare("select COUNT(*) from candidature where nom_compte = :new_username");
        $requete_doublonCandidature_byUsername->execute(array(
            ":new_username" => $_SESSION['username']
        ));
        $isUsernameDoublon = $requete_doublonCandidature_byUsername->fetch();
        if ($isUsernameDoublon[0] == 1) {
            $message['candidature_doublon'] = "Vous possédez déjà une candidature avec ce compte.";
        }

            // doublon candidature avec nom de groupe
        $requete_doublonCandidature_byGroupName = $db->prepare('select COUNT(*) from candidature where :new_groupname = nom_groupe');
        $requete_doublonCandidature_byGroupName->execute(array(
            ":new_groupname" => Flight::request()->data->group_name
        ));
        $isGroupnameDoublon = $requete_doublonCandidature_byGroupName->fetch();
        if ($isGroupnameDoublon[0] == 1) {
            $message['candidature_doublon'] = "Il existe déjà une candidature avec ce nom de groupe.";
        }

        // S'il la candidature n'est pas en double alors on peut valider le formulaire
        if (!isset($message['candidature_doublon'])) {
            //repertoire de destination pour les fichiers
            $rep_stockage = dirname('.') . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;

            //tableau associatif qui enregistre tous les fichiers envoyés au serveur
            $fichiers = array(
                "musique_1" => Flight::request()->files->fileMusic_1,
                "musique_2" => Flight::request()->files->fileMusic_2,
                "musique_3" => Flight::request()->files->fileMusic_3,
                "image_1" => Flight::request()->files->img_1,
                "image_2" => Flight::request()->files->img_2,
                "fiche_technique" => Flight::request()->files->f_tech,
                "dossier_presse" => Flight::request()->files->dossier_presse,
                "setlist_sacem" => Flight::request()->files->setlist
            );
            foreach ($fichiers as $type => $nom) {
                //nouveau nom de fichier complété grâce aux clés du tableau ou type fichier décrit quel est le fichier
                //exemple : 20201206_musique_2_michel050

                if(isset($nom)){//on vérifie que ke
                    $nouveau_nom = date("Y-m-d") . "_" . $type . "_" . $_SESSION['username'];
                    move_uploaded_file($nom['tmp_name'], $rep_stockage . $nouveau_nom);
                    $liste_nom_fichiers[] = $nouveau_nom;
                    //on ajoute au tableau le nouveau nom pour tous les fichiers
                }
            }
            $requete_saveCandidature = $db->prepare('insert into candidature values (:id,:nom_groupe,:dpt,:scene,:style,
            :annee_creation,:presentation,:experience,:representant_nom,:representant_prenom,:representant_adresse,
            :representant_cp,:representant_email,:representant_telephone,:membres,:youtube,:soundcloud,:site_fb,
            :producteur,:statut_associatif,:sacem,:nom_compte,:f_musique_1,:f_musique_2,:f_musique_3,:f_photos_1,:f_photos_2,
            :f_technique,:f_presse,:f_sacem_setlist)');

            $requete_saveCandidature->execute(array(
                ":id" => 0,
                ":nom_groupe" => Flight::request()->data->group_name,
                ":dpt" => Flight::request()->data->dpt,
                ":scene" => Flight::request()->data->type_scene,
                ":style" => Flight::request()->data->style_music,
                ":annee_creation" => Flight::request()->data->creation_year,
                ":presentation" => Flight::request()->data->presentation,
                ":experience" => Flight::request()->data->experience,
                ":representant_nom" => Flight::request()->data->leader_name,
                "representant_prenom" => Flight::request()->data->leader_prenom,
                "representant_adresse" => Flight::request()->data->leader_adress,
                "representant_cp" => Flight::request()->data->leader_codPost,
                "representant_email" => Flight::request()->data->leader_email,
                "representant_telephone" => Flight::request()->data->leader_phone,
                ":membres" => Flight::request()->data->info_membres,
                ":youtube" => Flight::request()->data->link_ytb,
                ":soundcloud" => Flight::request()->data->link_soundcloud,
                "site_fb" => Flight::request()->data->link_webFB,
                ":producteur" => Flight::request()->data->is_producteur,
                ":statut_associatif" => Flight::request()->data->statut_asso,
                ":sacem" => Flight::request()->data->inscrit_sacem,
                ":nom_compte" => $_SESSION['username'], //on stocke le nom de compte enregistré dans la variable de session
                "f_musique_1" => $liste_nom_fichiers[0],
                "f_musique_2" => $liste_nom_fichiers[1],
                "f_musique_3" => $liste_nom_fichiers[2],
                "f_photos_1" => $liste_nom_fichiers[3],
                "f_photos_2" => $liste_nom_fichiers[4],
                "f_technique" => $liste_nom_fichiers[5],
                "f_presse" => $liste_nom_fichiers[6],
                "f_sacem_setlist" => $liste_nom_fichiers[7]
            ));
            Flight::view()->assign('username',$_SESSION['username']);
            Flight::view()->display('./templates/candidature_success.tpl');
        } else {// Si doublon
            //Message différent selon la source du doublon : nom de compte ou nom de groupe

                //si une candidature existe avec ce compte : on ajoute la possibiliter de la voir avec un booléen
            $candidature_nomCompte = false;
            if ($isUsernameDoublon[0] == 1) {
               $source_doublon = 'Vous avez déjà une candidature enregistrée avec ce compte.';
               $candidature_nomCompte = true;
            }else{ //La source du doublon est le nom de groupe
                $source_doublon = "Une candidature existe déjà avec ce nom de groupe. Si vous pensez
                qu'il s'agit d'une erreur ou d'une usurpation, contactez les responsables.";
            }
            $data = array(
                "source" => $source_doublon,
                "source_nomCompte" => $candidature_nomCompte
            ); //on stocke dans un tableau les variables à envoyer
            Flight::render('./templates/candidature_doublon.tpl', $data);
        }
        //Si il y'a des erreurs de saisie -> contenu dans $message
    }else{
        if (!empty(Flight::request()->data->type_scene)) {
            $requete_nom_scene = $db->prepare('select nom from scene where id = :id');
            $requete_nom_scene->execute(array(
                ":id" => Flight::request()->data->type_scene
            ));
            //$nom_scene = $requete_nom_scene->fetch();
            $nom_scene = $requete_nom_scene->fetch();
            Flight::view()->assign('nom_scene', $nom_scene[0]);
        }
        $data = array(
            "scene" => $ligne_scene,
            "departement" => $ligne_dept,
            "message" => $message,
            "post" => $_POST
        );
        Flight::render('./templates/candidature_form.tpl', $data);
    }

});

Flight::route('/deconnexion', function(){
    session_destroy();
    Flight::redirect('/');
});

Flight::route('/profil', function(){
    Flight::view()->assign('titre','Profil');

    if(isset($_SESSION['username'])){ //L'utilisateur doit être connecté pour accéder à cette route
        $db = Flight::get('db');
        $requete_mail = $db->prepare('select email from utilisateur where nom = :nom');
        $requete_mail->execute(array(
            ":nom"=>$_SESSION['username']
        ));
        $result = $requete_mail->fetch();

        //test si utilisateur a une candidature enregistrée à son compte
        $requete_candidature = $db->prepare('select count(*) from candidature where candidature.nom_compte = :current_username');
        $requete_candidature->execute(array(
            ":current_username"=>$_SESSION['username'] //on compare le nom de compte enregistré en base pour X candidature avec
                                                       // celui de l'utilisateur connecté
        ));
        $resultat_candidature = $requete_candidature->fetch();
        $data = array(
            "email"=>$result[0],
            "username"=>$_SESSION['username'],
            "responsable"=>$_SESSION['responsable'][0],
            "nb_candidature"=>$resultat_candidature[0]
        );

        Flight::render('./templates/profil.tpl', $data); //Si connecté on affiche la page profil
    }else{
        Flight::redirect('/connexion'); //si pas connecté on le renvoie vers le formulaire de connexion
    }

});
Flight::route('/detail/@name', function($name){
    Flight::view()->assign('titre','Détail');

    if(isset($_SESSION['username'])){
        $db = Flight::get('db');
        $error=false; // permet de vérifier si la candidature existe

        //requete recupère toutes les infos de la candidature et le nom de scène grâce à une jointure
        $requete_infoCandidature = $db->prepare("select * from candidature,scene where (nom_compte = :nom_compte OR  nom_groupe = :nom_groupe) AND 
                                            candidature.scene = scene.id");
        $requete_infoCandidature->execute(array(
            ":nom_compte" => $name,
            ":nom_groupe" => $name
        ));
        $infoCandidature = $requete_infoCandidature->fetchAll();
        $data = array(
            "titre" => "Détail de candidature",
            "error" => $error
        );
        //On vérifie que une candidature existe avec les informations renseignées en paramètre : $name
        if(count($infoCandidature) === 0){
            $error = true;
        }else{
            $error = false;
        }
        $data = array(
            "titre" => "Détail de candidature",
            "error" => $error
        );
        //VÉRIFICATION : BLOQUER ACCÈS À UN UTILISATEUR QUI VEUT VISUALISER UNE CANDIDATURE QUI NE LUI APPARTIENT PAS
            //On vérifie si le nom de groupe de l'utilisateur courant différent de celui de la candidature demandée
        if(!$error){ //la candidature existe avec $name comme parametre
            Flight::view()->assign('infoCandidature',$infoCandidature[0]);

            $requete_groupCurrentUser = $db->prepare("select nom_groupe from candidature where nom_compte = :nom_currentUser");
            $requete_groupCurrentUser->execute(array(
                ":nom_currentUser" => $_SESSION['username']
            ));
            $groupCurrentUser = $requete_groupCurrentUser->fetchAll();

            //On regarde si l'utilisateur connecté a une candidature enregistrée : si non : ce n'est pas la sienne
            if(count($groupCurrentUser) === 0){ //l'utilisateur n'a pas de candidature à son nom
                if($_SESSION['responsable'][0] == 'o'){ // les reponsables ont accès à toutes les candidatures
                    Flight::render('./templates/detail_candidature.tpl', $data);
                }else{
                    Flight::view()->display('./templates/candidature_pasAccess.tpl');
                }
            }else{ //l'utilisateur courant a une candidature enregistrée
                if($groupCurrentUser[0][0] !== $infoCandidature[0][1]){//Si le nom de groupe appartenant à l'utilisateur courant
                                                              // est différent à celui de la candidature demandée : erreur

                    if($_SESSION['responsable'][0] == 'o'){ // les reponsables ont accès à toutes les candidatures
                        Flight::render('./templates/detail_candidature.tpl', $data); //on le renvoie vers le détail d'une candidature
                    }else{
                        Flight::view()->display('./templates/candidature_pasAccess.tpl');
                    }
                }else{
                    Flight::render('./templates/detail_candidature.tpl', $data);//l'utilisateur veut visualiser SA candidature
                }
            }
        }else{ //Si il y'a une erreur : candidature existe pas avec $name alors on affiche directement le template
            Flight::render('./templates/detail_candidature.tpl', $data);
        }

    }else{
        Flight::redirect('/connexion'); //l'utilisateur n'est pas connecté
    }
});

Flight::route('/liste', function(){
    Flight::view()->assign('titre','Liste des candidatures');

    //Accès si l'utilisateur est connecté et est responsable
    if(!isset($_SESSION['username'])){
        Flight::redirect('/connexion');
    } elseif ($_SESSION['responsable'][0] == 'n'){ //n'est pas responsable
        echo "<h1>Vous n'avez pas la permission d'accéder à cette ressource.</h1>";
    }else{
        $db = Flight::get('db');
        $requete_toutesCandidatures = $db->query('select * from candidature');
        $result = $requete_toutesCandidatures->fetchAll();

        $data = array(
            "liste" => $result,
        );

        Flight::render('./templates/liste_candidatures.tpl',$data);
    }
});
